# Packages needed for any development environment regardless of language

class dev_env::generic {
  # Operating system independent configurations
  package {
    'diffstat'   : ensure => installed;
    'subversion' : ensure => installed;
  }

  # Operating system dependent configurations
  case $::operatingsystem {
    'debian', 'ubuntu': {
      case $::lsbdistcodename {
        'squeeze': { package {'git':      ensure => present } }
        default:   { package {'git-core': ensure => installed }}
      }
      package {
        'build-essential'  : ensure => installed;
        'dash'             : ensure => installed;
        'debhelper'        : ensure => installed;
        'devscripts'       : ensure => installed;
        'devscripts-el'    : ensure => installed;
        'dh-make'          : ensure => installed;
        'dh-autoreconf'    : ensure => installed;
        'dpkg-dev-el'      : ensure => installed;
        'dput'             : ensure => installed;
        'dpatch'           : ensure => installed;
        'fakeroot'         : ensure => installed;
        'git-buildpackage' : ensure => installed;
        'gitk'             : ensure => installed;
        'gnupg'            : ensure => installed;
        'lintian'          : ensure => installed;
        'libkrb5-dev'      : ensure => installed;
        'libremctl-dev'    : ensure => installed;
        'libssl-dev'       : ensure => installed;
        'manpages-dev'     : ensure => installed;
        'module-assistant' : ensure => installed;
        'patchutils'       : ensure => installed;
        'pinentry-curses'  : ensure => installed;
        'quilt'            : ensure => installed;
        'reportbug'        : ensure => installed;
        'gnupg-agent':
            ensure  => installed,
            require => Package['pinentry-curses'];
      }
    }
    'redhat': {
      package {
        'e2fsprogs-devel'  : ensure => installed;
        'git'              : ensure => installed;
        'git-gui'          : ensure => installed;
        'rpm-build'        : ensure => installed;
        'unixODBC-devel'   : ensure => installed;  # openldap
      }
      # gnupg version change for rhel6 makes this more complex
      case $::lsbmajdistrelease {
        '6': {
          package { 'gnupg2': ensure => installed; }
        }
        default: {
          package { 'gnupg':  ensure => installed; }
        }
      }
    }
  }
}
