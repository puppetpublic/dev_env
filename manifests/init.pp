# Packages needed for development.  This should install a fairly complete
# development environment for building and testing code.  It's used on many of
# our test/dev systems where we may need to compile something, and is also
# used on the non-cardinal timeshares.

class dev_env {
    case $lsbdistcodename {
        'precise':  { include java::v6 }
        'oneiric':  { include java::v6 }
        'natty':    { include java::v6 }
        'karmic':   { include java::v6 }
        'lucid':    { include java::v6 }
        'squeeze':  { include java::v6 }
        'tikanga':  { include java::v6 }
        'santiago': { include java::v6 }
        'wheezy':   { include packages::java_7 }
        'jessie':   { include packages::java_7 }
        default:    { include java::v5 }
    }

    # Operating system independent configurations
    package {
        'autoconf'   : ensure => installed;
        'automake'   : ensure => installed;
        'bison'      : ensure => installed;
        'cvs'        : ensure => installed;
        'diffstat'   : ensure => installed;
        'flex'       : ensure => installed;
        'gdb'        : ensure => installed;
        'libtool'    : ensure => installed;
        'subversion' : ensure => installed;
    }

    # Operating system dependent configurations
    case $operatingsystem {
        'debian', 'ubuntu': {
            case $lsbdistcodename {
                'squeeze': { package {'git': ensure => present } }
                'jessie':  { package {'git': ensure => present } }
            }
            package {
                'build-essential'  : ensure => installed;
                'cdbs'             : ensure => installed;
                'dash'             : ensure => installed;
                'debootstrap'      : ensure => installed;
                'debhelper'        : ensure => installed;
                'debian-el'        : ensure => installed;
                'devscripts'       : ensure => installed;
                'devscripts-el'    : ensure => installed;
                'dh-make'          : ensure => installed;
                'dh-autoreconf'    : ensure => installed;
                'dpkg-dev-el'      : ensure => installed;
                'dput'             : ensure => installed;
                'dpatch'           : ensure => installed;
                'fakeroot'         : ensure => installed;
                'git-buildpackage' : ensure => installed;
                'git-core'         : ensure => installed;
                'git-cvs'          : ensure => installed;
                'git-svn'          : ensure => installed;
                'gitk'             : ensure => installed;
                'gnupg'            : ensure => installed;
                'kernel-package'   : ensure => installed;
                'lintian'          : ensure => installed;
                'libkrb5-dev'      : ensure => installed;
                'libremctl-dev'    : ensure => installed;
                'libssl-dev'       : ensure => installed;
                'libdb-dev'        : ensure => installed;
                'manpages-dev'     : ensure => installed;
                'module-assistant' : ensure => installed;
                'patchutils'       : ensure => installed;
                'pinentry-curses'  : ensure => installed;
                'python-stdeb'     : ensure => installed;
                'quilt'            : ensure => installed;
                'reportbug'        : ensure => installed;
                'svn-buildpackage' : ensure => installed;
                'valgrind'         : ensure => installed;
                'xbase-clients'    : ensure => installed;
                'gnupg-agent':
                    ensure  => installed,
                    require => Package['pinentry-curses'];
            }
        }
        'redhat': {
            package {
                'e2fsprogs-devel'  : ensure => installed;
                'gcc'              : ensure => installed;
                'git'              : ensure => installed;
                'git-gui'          : ensure => installed;
                'rpm-build'        : ensure => installed;
                'unixODBC-devel'   : ensure => installed;  # openldap
            }
            # gnupg version change for rhel6 makes this more complex
            case $lsbmajdistrelease {
                '6': {
                    package { 'gnupg2': ensure => installed; }
                }
                default: {
                    package { 'gnupg':  ensure => installed; }
                }
           }
        }
    }
}
